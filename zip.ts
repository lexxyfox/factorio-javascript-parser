import 'https://gitlab.com/snippets/2375964/raw/main/..ts' // iterator helpers polyfill
import { getNBytes, readExact } from 'https://deno.land/std/encoding/binary.ts'
import * as streams from 'https://deno.land/std/streams/conversion.ts'
import * as pathlib from 'https://deno.land/std/path/posix.ts'
import * as fsimpl from './fsimpl.ts'
import FileView from './fileview.ts'

// @ts-ignore:
const decode: (buffer: Uint8Array) => string = Deno.core.decode

export const EOCDR_OFFSET_MAX = -22
export const EOCDR_OFFSET_MIN = EOCDR_OFFSET_MAX - 2 ** 16 + 1

export const METHOD_STORE = 0
export const METHOD_SHRUNK = 1
export const METHOD_IMPLODE = 6
export const METHOD_DEFLATE = 8
export const METHOD_BZIP2 = 12
export const METHOD_LZMA = 14
export const METHOD_LZ77 = 19
export const METHOD_ZSTD = 93
export const METHOD_MP3 = 94
export const METHOD_XZ = 95
export const METHOD_JPEG = 96
export const METHOD_WAVPACK = 97

export type EOCDR = {
  length: number
  size: number
  offset: number
  comment: string
}

// Names are inspired by Python https://docs.python.org/3/library/zipfile.html#zipinfo-objects
export type SlimZipEntry = {
  filename: string
  header_offset: number
}

export type ZipEntry = SlimZipEntry & {
  extract_version: number
  flag_bits: number
  compress_type: number
  date_time: Date
  crc: number
  compress_size: number
  file_size: number
}

export const find_cd = async (
  file: Deno.Reader & Deno.Seeker,
): Promise<EOCDR> => {
  // don't go past the beginning of the file
  const seek_pos = Math.max(
    -(await file.seek(0, Deno.SeekMode.End)),
    EOCDR_OFFSET_MIN,
  )
  file.seek(seek_pos, Deno.SeekMode.End)
  const buf = await getNBytes(file, -seek_pos)

  // start reading from end of file and work backwards
  for (let i = -seek_pos + EOCDR_OFFSET_MAX; i > 0; i--) {
    // confirm EOCDR magic header
    if (buf[i] != 80 || buf[i + 1] != 75 || buf[i + 2] != 5 || buf[i + 3] != 6)
      continue

    const comment_len = buf[i + 20] + buf[i + 21] * 256

    // confirm EOCDR is at the very end of file
    // note: following code is probably wrong
    //if (-seek_pos - i !== -EOCDR_OFFSET_MAX - comment_len) continue

    return {
      length: buf[i + 8] + buf[i + 9] * 256,
      size:
        buf[i + 12] +
        buf[i + 13] * 256 +
        buf[i + 14] * 65536 +
        buf[i + 15] * 16777216,
      offset:
        buf[i + 16] +
        buf[i + 17] * 256 +
        buf[i + 18] * 65536 +
        buf[i + 19] * 16777216,
      comment:
        comment_len === 0
          ? ''
          : decode(buf.slice(i + 22, i + 22 + comment_len)),
    }
  }

  throw 'Cannot locate ZIP End Of Central Directory Record!'
}

export const parse_cd_fast = async function* (
  file: Deno.Reader & Deno.Seeker,
  eocdr?: EOCDR,
): AsyncGenerator<SlimZipEntry, void, never> {
  eocdr ??= await find_cd(file)

  await file.seek(eocdr.offset, 0)
  const data = new Uint8Array(46)

  for (let i = 0; i < eocdr.length; i++) {
    await readExact(file, data)

    yield {
      filename: decode(await getNBytes(file, data[28] + data[29] * 256)),
      header_offset:
        data[42] + data[43] * 256 + data[44] * 65536 + data[45] * 16777216,
    }

    // skip extra data field and comment
    await file.seek(data[30] + data[32] + (data[31] + data[33]) * 256, 1)
  }
}

/**
 * @param file A handle of the ZIP file from which to read from.
 * @param offset The byte offset (from the beginning of the ZIP file) of the local file header.
 * @returns a read-only view of the decompressed contents of the ZIP entry found at the specified offset.
 * @throws "Unsupported compression type" if compression type isn't yet supported.
 */
export const decompress_at = async (file: fsimpl.ReadableFile, offset: number) => {
  await file.seek(offset + 8, 0)
  const data = new Uint8Array(22)
  await readExact(file, data)

  const compress_type = data[0] + data[1] * 256
  const compress_size =
    data[10] + data[11] * 256 + data[12] * 65536 + data[13] * 16777216

  // skip file name and extra data field
  const view = new FileView(
    file,
    offset + 30 + data[18] + data[20] + (data[19] + data[21]) * 256,
    compress_size,
  )

  switch (compress_type) {
    case METHOD_STORE:
      return view
    case METHOD_DEFLATE:
      const decompressed = streams.readerFromStreamReader(
        streams
          .readableStreamFromReader(view)
          .pipeThrough(new DecompressionStream('deflate-raw'))
          .getReader(),
      ) as fsimpl.ReadableFile
      decompressed.close = view.close
      decompressed.rid = view.rid
      return decompressed
  }
  throw `Unsupported compression type ${compress_type}`
}

const read_only = { read: true, write: false }

export const RandomAccessZipReader = class
  implements
    Deno.Closer,
    fsimpl.DirReader,
    fsimpl.FileReader,
    fsimpl.Opener<fsimpl.ReadableFile>,
    fsimpl.TextReader
{
  #path?: string
  #filesystem?: fsimpl.Opener<fsimpl.ReadableFile>
  #initialisation?: Promise<void>
  infolist!: SlimZipEntry[]
  comment!: string

  constructor(path: string, filesystem: fsimpl.Opener<fsimpl.ReadableFile> = Deno) {
    this.#path = path
    this.#filesystem = filesystem
    this.#initialisation = this.#initialise()
  }

  #initialise = async () => {
    const file = await this.#filesystem!.open(this.#path!, read_only)
    const eocdr = await find_cd(file)
    this.comment = eocdr.comment
    // @ts-ignore: This was working fine yesterday without an error?!?!
    this.infolist = await parse_cd_fast(file, eocdr).toArray()
    file.close()
  }

  close = () => {
    this.#path = this.#filesystem = this.#initialisation = undefined
    this.infolist = []
  }

  open = async (path: string | URL | number = 0) => {
    if (this.#filesystem === undefined || this.#path === undefined)
      throw new Deno.errors.BadResource('Bad resource ID')
    const file = await this.#filesystem.open(this.#path, read_only)
    await this.#initialisation
    if (typeof path === 'number')
      return decompress_at(file, this.infolist[path].header_offset)
    if (path instanceof URL) {
      if (path.protocol !== 'file:') throw new TypeError('Must be a file URL.')
      path = pathlib.fromFileUrl(path).slice(1)
    }
    for (const entry of this.infolist)
      if (entry.filename === path)
        return decompress_at(file, entry.header_offset)
    throw new Deno.errors.NotFound(
      "No such file or directory, open '" + path + "'",
    )
  }

  readFile = async (path: string | URL | number = 0) => {
    const file = await this.open(path)
    const data = await streams.readAll(file)
    //file.close() // hlep
    return data
  }

  readTextFile = async (path: string | URL | number = 0) =>
    decode(await this.readFile(path))

  public async *readDir(path: string | URL | number = '') {
    if (typeof path === 'number') path = this.infolist[path].filename
    else if (path instanceof URL) {
      if (path.protocol !== 'file:') throw new TypeError('Must be a file URL.')
      path = pathlib.fromFileUrl(path).slice(1)
    }

    path = pathlib.normalize(path)
    if (!path.endsWith('/')) path += '/'
    if (path.startsWith('/')) path = path.slice(1)
    if (path === './') path = ''
    const path_part_len = path === '' ? 1 : path.split('/').length
    const found: { [K in string]: boolean } = {}
    await this.#initialisation

    for (const entry of this.infolist)
      if (entry.filename.startsWith(path)) {
        const entry_parts = entry.filename.split('/'),
          name = entry_parts[path_part_len - 1]
        if (entry_parts.length == path_part_len && name !== '')
          found[name] ??= false
        else if (entry_parts.length > path_part_len) found[name] ??= true
      }

    for (const name in found) {
      const isDirectory = found[name]
      yield {
        isDirectory,
        isFile: !isDirectory,
        isSymlink: false,
        name,
      } as Deno.DirEntry
    }
  }
}
