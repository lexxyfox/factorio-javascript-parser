#!/usr/bin/env -S deno run --no-check --allow-read

import 'https://gitlab.com/snippets/2375964/raw/main/..ts' // iterator helpers polyfill
import * as pathlib from 'https://deno.land/std/path/posix.ts'
import * as zip from './zip.ts'
import * as mods from './mods.ts'

// @ts-ignore:
const decode: (buffer: Uint8Array) => string = Deno.core.decode


console.log(Deno.resources())

//const path = '/home/lexxy/Games/Factorio/mods/factoriohd_logistics_0.0.8.zip'

const path = '/home/lexxy/Games/Factorio/mods/foxxy_modpack_0.0.3.zip'
//const path = '/home/lexxy/Downloads/factorio-test/data/base/'
const modfile = await mods.open(path)

console.log(modfile)

//console.log(await modfile.readTextFile('info.json'))

console.log(Deno.resources())
