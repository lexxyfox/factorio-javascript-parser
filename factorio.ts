import { getpwuid } from './unix/fake_pwd.ts'
import * as path from 'https://deno.land/std/path/posix.ts'
import * as procfs from './unix/procfs.ts'
import Logger from './logger.ts'
import readTextFile from './readTextFile.ts'
import * as fsimpl from './fsimpl.ts'
import parse_ini from 'https://gitlab.com/snippets/2376767/raw/main/..ts'

const get_home_dir = async () => (await getpwuid())!.dir

// but what about "__PATH__system-write-data__"??

// returns value of "__PATH__system-write-data__"
export const get_system_write_path = async () => get_home_dir() + '/.factorio'

// returns value of "__PATH__system-read-data__"
export const system_read_path = '/usr/share/factorio'

export const operating_system = async (
  filesystem:
    | fsimpl.Opener
    | fsimpl.FileReader
    | fsimpl.TextReader = Deno,
) => {
  // note: Factorio actually just calls the command `lsb_release` and parses it with colons and all.
  //   Slightly better would be to call `lsb_release -irs` and then replace \n with space.
  //   But even better, lsb_release is a python script that calls `lsb_release.get_distro_information()`
  //   which in turn just reads from /etc/lsb-release.
  //   Somehow I think just reading from /etc/lsb-release will be faster,
  //   and also doesn't require run permissions - so I won't even try.
  let id, rel
  for (const line of (await readTextFile('/etc/lsb-release', filesystem))
    .trim()
    .split('\n')) {
    const [key, val] = line.split('=')
    switch (key) {
      case 'DISTRIB_ID':
        id = val
        break
      case 'DISTRIB_RELEASE':
        rel = val
        break
    }
    if (id && rel) return `Linux (${id} ${rel})`
  }
}

export const system_info = async (filesystem: fsimpl.Opener = Deno) => {
  // note: Factorio actually gets cpu count from /sys/devices/system/cpu/online,
  //   but I'm using /proc/cpuinfo to avoid opening another file, especially one from /sys
  let [cpuinfo, meminfo] = await Promise.all([
    procfs.cpuinfo(filesystem).next(),
    procfs.meminfo(filesystem),
  ])
  return `[CPU: ${cpuinfo.value!.model_name}, ${
    cpuinfo.value!.siblings
  } cores, RAM: ${Math.floor(meminfo.MemTotal / 1048576)} MB]`
}

const env_var_names = [
  'DISPLAY',
  'WAYLAND_DISPLAY',
  'DESKTOP_SESSION',
  'XDG_SESSION_DESKTOP',
  'XDG_CURRENT_DESKTOP',
  '__GL_FSAA_MODE',
  '__GL_LOG_MAX_ANISO',
  '__GL_SYNC_TO_VBLANK',
  '__GL_SORT_FBCONFIGS',
  '__GL_YIELD',
]

export const environment = () => {
  const vals = Array(env_var_names.length)
  for (
    let i = 0, l = vals.length, k, v;
    i < l;
    k = env_var_names[i],
      v = Deno.env.get(k),
      vals[i] = k + '=' + (v === undefined ? '<unset>' : v),
      ++i
  ) {}
  return vals.join(' ')
}

const arg0 = async (
  filesystem:
    | fsimpl.Opener
    | fsimpl.FileReader
    | fsimpl.TextReader = Deno,
) => (await readTextFile('/proc/self/cmdline', filesystem)).split('\0')[0]

const parse_cfg = async (
  path: string,
  filesystem:
    | fsimpl.Opener
    | fsimpl.FileReader
    | fsimpl.TextReader = Deno,
) => {
  const data: Record<string, string> = {}
  for (const line of (await readTextFile(path, filesystem)).split('\n'))
    if (line && !line.startsWith('#')) {
      const pos = line.indexOf('=')
      if (pos > 0) data[line.slice(0, pos)] = line.slice(pos + 1)
    }
  return data
}

// Factorio actually uses statfs(), but this trades an ffi call for a run command,
//   which is not only less scary, but is easier to work across the network.
export const diskusage = async (
  path: string,
  filesystem: fsimpl.Runner = Deno,
) => {
  try {
    const p = filesystem.run({
      cmd: ['df', '-B1', '--output=avail,size', path],
      stdout: 'piped',
      stderr: 'piped',
    })
    const [status, out, err] = await Promise.all([
      p.status(),
      p.output(),
      p.stderrOutput(),
    ])
    if (status.success) {
      const vals = new TextDecoder().decode(out).split('\n')[1].split(' ')
      return `[${Math.floor(parseInt(vals[0]) / 1048576)}/${Math.floor(
        parseInt(vals[1]) / 1048576,
      )}MB]`
    }
  } catch {}
  return '[/MB]'
}

/**
 *
 * @returns true if given path is either a url with a file: protocol, or potentially a valid path
 */
const path_is_filesystem = (path: string) => {
  try {
    return new URL(path).protocol === 'file:'
  } catch {
    return true
  }
}

export const parse_bool = (str: string) => str.toLocaleLowerCase() === 'true'

type ProfileOptions = {
  /**
   * The filesystem to operate on. Defaults to the local filesystem.
   */
  filesystem?: fsimpl.Opener &
    fsimpl.DirReader &
    Partial<fsimpl.FileReader> &
    Partial<fsimpl.Runner>

  /**
   * Corresponds to Factorio's `--executable-path` argument and also `__PATH__executable__`, the directory that contains Factorio's executable.
   *
   * If undefined and `Deno.mainModule` is a file, is assumed to be the directory that contains `Deno.mainModule`.
   *
   * If undefined but `Deno.mainModule` isn't a file, is assumed to be the current working directory.
   */
  executable_path?: string

  /**
   * Corresponds to Factorio's `--config` argument.
   *
   * Defaults to `__PATH__executable__/../../config-path.cfg`
   */
  config?: string

  /**
   * Corresponds to `config-path` in config-path.cfg. Should contain a file named config.ini.
   *
   * Defaults to `__PATH__executable__/../../config`
   */
  config_path?: string

  /**
   * Corresponds to the `read-data` property in the `path` section of `config.ini`.
   */
  read_data_path?: string

  /**
   * Corresponds to the `write-data` property in the `path` section of `config.ini`.
   */
  write_data_path?: string

  /**
   * Corresponds to Factorio's `--mod-directory` argument.
   * */
  mod_directory?: string

  verbose?: boolean
}

export const Profile = class {
  filesystem!: fsimpl.Opener &
    fsimpl.DirReader &
    Partial<fsimpl.FileReader> &
    Partial<fsimpl.Runner>
  executable_path!: string
  config_path!: string
  read_data_path!: string
  write_data_path!: string
  mod_directory!: string
  config: { enable_new_mods: boolean }

  constructor() {
    const logger = new Logger(
      'Factorio 1.1.63 (build 59940, linux64, headless)',
    )
    this.log = logger.log
    this.config = { enable_new_mods: true }
  }

  log: (msg: string) => void

  parse_path = async (p: string) => {
    if (p.startsWith('__PATH__executable__') && this.executable_path)
      return path.join(this.executable_path, p.slice(20))
    if (p.startsWith('__PATH__system-read-data__'))
      return path.join(system_read_path, p.slice(26))
    if (p.startsWith('__PATH__system-write-data__'))
      return path.join(await get_system_write_path(), p.slice(27))
    return p
  }

  /**
   * Returns a set of mod ids that have been specificly enabled in mod-list.json.
   * Does not include new mods that haven't yet been enabled.
   */
  get_enabled_mods = async () => {
    const enabled = new Set<string>()

    for (const mod of JSON.parse(
      await readTextFile(
        path.join(this.mod_directory, 'mod-list.json'),
        this.filesystem,
      ),
    ).mods)
      if (mod.enabled) enabled.add(mod.name)

    return enabled
  }

  static new = async (
    path_to_exe?: string | null,
    opts: ProfileOptions = {},
  ) => {
    const inst = new this()
    Object.defineProperty(inst, 'filesystem', {
      value: opts.filesystem ?? Deno,
      enumerable: false,
      writable: false,
    })
    inst.log('Operating system: ' + (await operating_system(inst.filesystem)))

    const system_info_str = system_info(inst.filesystem)

    const executing_locally = path_is_filesystem(Deno.mainModule)
    path_to_exe ??= path.relative(Deno.cwd(), path.fromFileUrl(Deno.mainModule))

    // this is just for show (in console output)
    let fake_args = [
      executing_locally ? path_to_exe : await arg0(inst.filesystem),
    ]
    if (opts.executable_path)
      fake_args.push('--executable-path', opts.executable_path)
    if (opts.config_path) fake_args.push('--config', opts.config_path)
    if ('verbose' in opts && opts.verbose) fake_args.push('--verbose')
    if (opts.mod_directory)
      fake_args.push('--mod-directory', opts.mod_directory)
    inst.log(
      'Program arguments: ' + fake_args.map(x => '"' + x + '"').join(' '),
    )

    inst.executable_path =
      opts.executable_path ??
      (executing_locally ? path.dirname(path_to_exe) : Deno.cwd())

    opts.config ??= await inst.parse_path(
      '__PATH__executable__/../../config-path.cfg',
    )

    inst.config_path = await inst.parse_path(
      opts.config_path ??
        (
          await parse_cfg(opts.config, inst.filesystem)
        )['config-path'] ??
        '__PATH__executable__/../../config',
    )

    // todo: only try if read_data_path or write_data_path are required
    const config_data = await parse_ini(
      await readTextFile(
        path.join(inst.config_path, 'config.ini'),
        inst.filesystem,
      ),
    )

    if (config_data.other['enable-new-mods'])
      inst.config.enable_new_mods = parse_bool(
        config_data.other['enable-new-mods'],
      )

    inst.read_data_path = path.resolve(
      opts.read_data_path ??
        (await inst.parse_path(config_data.path['read-data'])),
    )
    inst.write_data_path = path.resolve(
      opts.write_data_path ??
        (await inst.parse_path(config_data.path['write-data'])),
    )

    const diskusage_str =
      'run' in inst.filesystem
        ? diskusage(inst.write_data_path, inst.filesystem as fsimpl.Runner)
        : '[/MB]'

    inst.log('Read data path: ' + inst.read_data_path)
    inst.log(`Write data path: ${inst.write_data_path} ${await diskusage_str}`)
    inst.log(
      'Binaries path: ' + path.resolve(path.join(inst.executable_path, '..')),
    )
    inst.log('System info: ' + (await system_info_str))
    inst.log('Environment: ' + environment())
    inst.log('Running in headless mode')

    inst.mod_directory =
      opts.mod_directory ?? path.join(inst.write_data_path, 'mods')

    return inst
  }
}
