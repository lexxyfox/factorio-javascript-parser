# Path parsing

* If a path begins with `__PATH__executable__`, it is replaced with its value
* If a path begins with `__PATH__system-read-data__`, it is replaced with "/usr/share/factorio"
* If a path begins with `__PATH__system-write-data__`, it is replaced with ~/".factorio" Note: NOT $HOME, NOT $USER. Here, `~` refers to "getpwuid(getuid())->pw_dir"
* `~` or any of its friends are not expanded

# Factorio boot sequence

1. `__PATH__executable__` is set to the path one directory above the path provided by `--executable-path` if provided (NOT parsed using the rules above), otherwise it is set to the path containing the executable being run.
1. If `--config` is provided, set the path to Factorio's config file to the path provided (referred to here as `CONFIG`). If it is not provided:
   1. `__PATH__executable__/../../config-path.cfg` is read as a key=val #comment file.
   1. The key `config-path` is read from `config-path.cfg`. It is parsed as per the path rules above, the string `/config.ini` is appended, then it is assigned to `CONFIG`.
   1. If it does not yet exist, initialize it with some stuff (TODO).
1. Read `CONFIG` as an INI file.
1. `READ_PATH` is set to the value of `CONFIG:path.read-data`. `WRITE_PATH` is set to the value of `CONFIG:path.write-data`.
1. 

