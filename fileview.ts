import Resource from 'https://gitlab.com/snippets/2378298/raw/main/..ts'

// @ts-ignore:
export default class FileView<
  F extends Deno.Seeker &
    Partial<Resource> &
    Partial<Deno.Reader> &
    Partial<Deno.Closer>,
> implements F
{
  #file: F
  #offset: number
  #length?: number
  #initialisation: Promise<number>

  constructor(file: F, byteOffset = 0, byteLength?: number) {
    if (typeof byteLength !== 'undefined' && byteLength < 0)
      throw `RangeError: Invalid length ${byteLength}`
    this.#initialisation = file.seek(byteOffset, 0)
    this.#file = file
    this.#offset = byteOffset
    this.#length = byteLength
  }

  get rid() {
    return this.#file.rid
  }

  close = () => this.#file.close!()

  seek = async (offset: number, whence: Deno.SeekMode) => {
    await this.#initialisation
    switch (whence) {
      case Deno.SeekMode.Start:
        return (
          (await this.#file.seek(this.#offset + offset, whence)) - this.#offset
        )
      case Deno.SeekMode.Current:
        return (await this.#file.seek(offset, whence)) - this.#offset
      case Deno.SeekMode.End:
        if (typeof this.#length !== 'undefined')
          return (
            (await this.#file.seek(
              this.#offset + this.#length + offset,
              Deno.SeekMode.Start,
            )) - this.#offset
          )
        return (await this.#file.seek(offset, whence)) - this.#offset
    }
    throw `TypeError: Invalid seek mode: ${whence}`
  }

  read = async (p: Uint8Array) => {
    await this.#initialisation
    if (typeof this.#length === 'undefined') return await this.#file.read!(p)
    const to_read = Math.min(
      p.byteLength,
      this.#offset + this.#length - (await this.#file.seek(0, 1)),
    )
    if (to_read === 0) return null // eof
    const buffer = new Uint8Array(to_read)
    const read = await this.#file.read!(buffer)
    p.set(buffer)
    return read
  }
}
