
import * as fsimpl from '../fsimpl.ts'
import readTextFile from '../readTextFile.ts'

// PRIVATE STUFF. Highly unstable, don't use!



const num_procs = async (filesystem: fsimpl.DirReader = Deno, path = '/proc') => {
  let n = 0
  for await (const entry of filesystem.readDir(path))
    if (entry.isDirectory)
      n++
  return n
}

const cpuinfo_int_keys = new Set(['cpu_family', 'model', 'stepping', 'physical_id', 'siblings', 'core_id', 'cpu_cores', 'apicid', 'initial_apicid', 'cpuid_level', 'clflush_size', 'cache_alignment', 'microcode'])
const cpuinfo_bool_keys = new Set(['fpu', 'fpu_exception', 'wp'])
const cpuinfo_set_keys = new Set(['flags', 'bugs'])
const cpuinfo_float_keys = new Set(['bogomips'])


// PROCEDURAL INTERFACE. Defaults to current system. Probably stable.

export const cmdline = async (filesystem: fsimpl.Opener = Deno, path = '/proc/cmdline') =>
  (await readTextFile(path, filesystem)).slice(0, -1)

/** Shows registered system console lines. */
export const consoles = async function*(filesystem: fsimpl.Opener = Deno, path = '/proc/consoles') {
  for (const line of (await readTextFile(path, filesystem)).split('\n'))
    if (line)
      yield {
        device: line.slice(0, 20).trim(),
        read: line[21] !== '-',
        write: line[22] !== '-',
        unblank: line[23] !== '-',
        flags: {
          enabled: line[26] !== ' ',
          consdev: line[27] !== ' ',
          boot: line[28] !== ' ',
          printbuffer: line[29] !== ' ',
          brl: line[30] !== ' ',
          anytime: line[31] !== ' ',
        },
        major: parseInt(line.slice(33, 38)),
        minor: parseInt(line.slice(39)),
      }
}

// https://elixir.bootlin.com/linux/latest/source/arch/x86/kernel/cpu/proc.c#L61
export const cpuinfo = async function*(filesystem: fsimpl.Opener = Deno, path = '/proc/cpuinfo') {
  for (const cpu_text of (await readTextFile(path, filesystem)).split('\n\n')) 
    if (cpu_text) {
      const cpu: Record<string, any> = {}
      for (const cpu_line of cpu_text.split('\n')) {
        const pos = cpu_line.indexOf(':')
        const key = cpu_line.slice(0, pos).trim().replaceAll(' ', '_').toLowerCase()
        const val = cpu_line.slice(pos + 1).trim()
        if (cpuinfo_int_keys.has(key))
          cpu[key] = parseInt(val)
        else if (cpuinfo_bool_keys.has(key))
          cpu[key] = val === 'yes'
        else if (cpuinfo_set_keys.has(key))
          cpu[key] = new Set(val.split(' '))
        else if (cpuinfo_float_keys.has(key))
          cpu[key] = parseFloat(val)
        else if (key === 'processor')
          continue
        else if (key === 'cpu_mhz') 
          cpu['cpu_khz'] = parseFloat(val) * 1000
        else if (key === 'cache_size')
          cpu[key] = val.endsWith('KB') ? parseInt(val.slice(0, -2)) * 1024 : val
        else if (key === 'tlb_size')
          cpu[key] = parseInt(val.slice(0, -9))
        else if (key === 'address_sizes') {
          cpu['address_sizes'] = {}
          for (const spec of val.split(',')) {
            const parts = spec.trim().split(' ')
            cpu['address_sizes'][parts[2]] = parseInt(parts[0])
          }
        } else if (key === 'power_management') {
          cpu['power_management'] = new Set()
          for (const x of val.split(' '))
            cpu['power_management'].add(
              x.startsWith('[') ? parseInt(x.slice(1, -1)) : x
            )
        } else
          cpu[key] = val
      }
      yield cpu
    }
}

export const filesystems = async (
  filesystem: fsimpl.Opener = Deno,
  path = '/proc/filesystems',
) => {
  const list: Record<string, boolean> = {}
  for (const line of (await readTextFile(path)).split('\n'))
    if(line) {
      const [nodev, name] = line.split('\t')
      list[name] = nodev !== ''
    }
  return list
}

export const loadavg = async (
  filesystem: fsimpl.Opener = Deno,
  path = '/proc/loadavg',
) => {
  const parts = (await readTextFile(path, filesystem)).split(' ')
  const pos = parts[3].indexOf('/')
  return {
    avenrun: [parseFloat(parts[0]), parseFloat(parts[1]), parseFloat(parts[2])],
    nr_running: parseInt(parts[3].slice(0, pos)),
    nr_threads: parseInt(parts[3].slice(pos + 1)),
    idr_cursor: parseInt(parts[4]),
  }
}

export const meminfo = async (
  filesystem: fsimpl.Opener = Deno,
  path = '/proc/meminfo',
) => {
  const text = await readTextFile(path, filesystem)
  const info: Record<string, number> = {}
  for (const line of text.split('\n'))
    if (line) {
      const pos = line.indexOf(':')
      const key = line.slice(0, pos).trim()
      let val = line.slice(pos + 1).trim()
      info[key] = val.endsWith('kB')
        ? parseInt(val.slice(0, -2)) * 1024
        : parseInt(val)
    }

  return info
}

// https://elixir.bootlin.com/linux/latest/source/block/genhd.c#L826
export const partitions = async function* (
  filesystem: fsimpl.Opener = Deno,
  path = '/proc/partitions',
) {
  const lines = (await readTextFile(path, filesystem)).split('\n').slice(2, -1)
  for (const line of lines)
    yield {
      name: line.substring(25),
      blocks: parseInt(line.substring(13, 24)),
      major: parseInt(line.substring(0, 4)),
      minor: parseInt(line.substring(4, 13)),
    }
}

export const uptime = async (filesystem: fsimpl.Opener = Deno, path = '/proc/uptime') => {
  const parts = (await readTextFile(path, filesystem)).split(' ')
  return {
    uptime: parseFloat(parts[0]),
    idle: parseFloat(parts[1]),
  }
}

// FAKE SYSCALL EMULATION. Functions are probably stable,
// ...BUT might be moved to another module.

export async function avenrun(filesystem: fsimpl.Opener = Deno) {
  const loads = (await loadavg(filesystem)).avenrun
  return [
    Math.round(loads[0] * 200),
    Math.round(loads[1] * 200),
    Math.round(loads[2] * 200),
  ]
}

// https://man7.org/linux/man-pages/man2/sysinfo.2.html
export async function sysinfo(filesystem: fsimpl.Opener & fsimpl.DirReader = Deno) {
  const [mi, up, la, procs] = await Promise.all([
    meminfo(filesystem),
    uptime(filesystem),
    loadavg(filesystem),
    num_procs(filesystem),
  ])
  return {
    uptime   : up.uptime,
    loads    : la.avenrun,
    totalram : mi.MemTotal,
    freeram  : mi.MemFree,
    sharedram: mi.Shmem,
    bufferram: mi.Buffers,
    totalswap: mi.SwapTotal,
    freeswap : mi.SwapFree,
    procs    : procs,
    totalhigh: mi.HighTotal,
    freehigh : mi.HighFree,
    mem_unit : 1,
  }
}

// OBJECT-ORIENTED INTERFACE. Use with remote systems.

export class ProcFS {
  #filesystem
  #path
  constructor(path = '/proc', filesystem: fsimpl.Opener = Deno) {
    this.#filesystem = filesystem
    this.#path = path
  }
  consoles = () => cmdline(this.#filesystem, this.#path + '/consoles')
  cmdline = () => cmdline(this.#filesystem, this.#path + '/cmdline')
  cpuinfo = () => cpuinfo(this.#filesystem, this.#path + '/cpuinfo')
  filesystems = () => cpuinfo(this.#filesystem, this.#path + '/filesystems')
  loadavg = () => loadavg(this.#filesystem, this.#path + '/loadavg')
  meminfo = () => meminfo(this.#filesystem, this.#path + '/meminfo')
  partitions = () => partitions(this.#filesystem, this.#path + '/partitions')
  uptime = () => uptime(this.#filesystem, this.#path + '/uptime')
}