import { getuid } from './unistd.ts'

export const getpwents = async function*() {
  for (const line of (await Deno.readTextFile('/etc/passwd')).split('\n')) 
    if (line.length > 0) {
      const parts = line.split(':')
      yield {
        'name'  : parts[0],
        'passwd': parts[1],
        'uid'   : parseInt(parts[2]),
        'gid'   : parseInt(parts[3]),
        'gecos' : parts[4].length > 0 ? parts[4] : null,
        'dir'   : parts[5],
        'shell' : parts[6],
      }
    }
}

export const getpwuid = async (uid = getuid()) => {
  for await (const pwent of getpwents())
    if (uid === pwent.uid)
      return pwent
  return null
}
