import * as pathlib from 'https://deno.land/std/path/posix.ts'
import * as fsimpl from './fsimpl.ts'
import * as zip from './zip.ts'
import readTextFile from './readTextFile.ts'
import * as ver from './version.ts'

export const ModPackageReadError = class extends Error {}

export const LocalMod = class
  implements
    Deno.Closer,
    fsimpl.DirReader,
    fsimpl.FileReader,
    fsimpl.Opener<fsimpl.ReadableFile>,
    fsimpl.TextReader
{
  #path
  #filesystem
  name: string
  version: InstanceType<typeof ver.Version>
  title?: string
  author?: string
  contact?: string
  homepage?: string
  description?: string
  factorio_version: InstanceType<typeof ver.Version>

  constructor(
    path: string = '.',
    filesystem: fsimpl.DirReader &
      fsimpl.FileReader &
      fsimpl.Opener<fsimpl.ReadableFile> &
      Partial<fsimpl.TextReader> = Deno,
    info: any = {},
  ) {
    this.#path = path + '/'
    this.#filesystem = filesystem
    this.name = info.name
    this.version = ver.parse(info.version)
    this.title = info.title
    this.author = info.author
    this.contact = info.contact
    this.homepage = info.homepage
    this.description = info.description
    this.factorio_version = ver.parse(info.factorio_version) ?? ver.DEFAULT
  }

  close = () => {}

  readDir = (path: string | URL) => {
    if (path instanceof URL) throw 'Oops, not implemented yet!'
    return this.#filesystem.readDir(this.#path + path)
  }

  open = (path: string | URL, options?: Deno.OpenOptions | undefined) => {
    if (path instanceof URL) throw 'Oops, not implemented yet!'
    return this.#filesystem.open(this.#path + path, options)
  }

  readFile = (
    path: string | URL,
    options?: Deno.ReadFileOptions | undefined,
  ) => {
    if (path instanceof URL) throw 'Oops, not implemented yet!'
    return this.#filesystem.readFile(this.#path + path, options)
  }

  readTextFile = (
    path: string | URL,
    options?: Deno.ReadFileOptions | undefined,
  ) => {
    if (path instanceof URL) throw 'Oops, not implemented yet!'
    return readTextFile(this.#path + path, this.#filesystem)
  }
}

export const from_zip_file = async (
  path: string,
  filesystem: fsimpl.Opener<fsimpl.ReadableFile> = Deno,
) => {
  const zipfile = new zip.RandomAccessZipReader(path, filesystem)
  for await (const ent of zipfile.readDir(''))
    if (ent.isDirectory)
      try {
        const info = JSON.parse(
          await zipfile.readTextFile(ent.name + '/info.json'),
        )
        return new LocalMod(ent.name, zipfile, info)
      } catch {
        continue
      }
  throw new ModPackageReadError()
}

export const from_folder = async (
  path: string,
  filesystem: fsimpl.DirReader &
    fsimpl.FileReader &
    fsimpl.Opener<fsimpl.ReadableFile> &
    Partial<fsimpl.TextReader> = Deno,
) => {
  const info = JSON.parse(await readTextFile(path + '/info.json', filesystem))
  return new LocalMod(path, filesystem, info)
}

export const open = async (
  path: string,
  filesystem: fsimpl.DirReader &
    fsimpl.FileReader &
    fsimpl.Opener<fsimpl.ReadableFile> = Deno,
) => {
  // I want to avoid adding additional dependencies (such as lstat) or faulty guessing, so I "just do it"
  try {
    return await from_zip_file(path, filesystem)
  } catch {
    return from_folder(path, filesystem)
  }
}
