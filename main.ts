#!/usr/bin/env -S deno run --no-check --allow-read --allow-env --allow-run

import 'https://gitlab.com/snippets/2375964/raw/main/..ts' // iterator helpers polyfill
import * as factorio from './factorio.ts'
import * as path from 'https://deno.land/std/path/posix.ts'
import * as procfs from './unix/procfs.ts'
import readTextFile from './readTextFile.ts'

//console.log(await procfs.meminfo())
//console.log(await procfs.sysinfo())
//console.log((await procfs.cpuinfo().next()).value)

/*
const filesystems = await procfs.filesystems()
console.log(Object.keys(filesystems))
*/

const executable_path = path.relative(Deno.cwd(), '/home/lexxy/Games/Factorio/bin/x64')
const inst = await factorio.Profile.new(null, {executable_path: executable_path, verbose: true})

console.log()
console.log(inst)

console.log(await inst.get_enabled_mods())