#!/usr/bin/env -S deno run --no-check --allow-read

import 'https://gitlab.com/snippets/2375964/raw/main/..ts' // iterator helpers polyfill
import { getNBytes, readExact } from 'https://deno.land/std/encoding/binary.ts'
import * as pathlib from 'https://deno.land/std/path/posix.ts'
import * as zip from './zip.ts'
import * as streams from 'https://deno.land/std/streams/conversion.ts'
import FileView from './fileview.ts'
import { fromFileUrl } from 'https://deno.land/std/path/posix.ts'

// @ts-ignore:
const decode: (buffer: Uint8Array) => string = Deno.core.decode

//const path = '/home/lexxy/Downloads/avorion pack/server/avorion-server.zip'

const get_mod_info = async (path: string) => {
  const zipfile = new zip.RandomAccessZipReader(path)
  try {
    for await (const ent of zipfile.readDir(''))
      if (ent.isDirectory)
        return JSON.parse(await zipfile.readTextFile(ent.name + '/info.json'))
  } finally {
    zipfile.close()
  }
}

const fulfilled = (promise: PromiseSettledResult<any>) =>
  promise.status === 'fulfilled'

const get_value = (promise: PromiseFulfilledResult<Object>) => promise.value

const path = '/home/lexxy/.factorio/mods'

// this took ~1.3 seconds with old readDir() code
// with new code it takes ~1.1 seconds
// still kinda slow :/
const with_base = (ent: Deno.DirEntry) => pathlib.join(path, ent.name)
const foo = (
  await Promise.allSettled(
    (
      // @ts-ignore: This was working fine yesterday without an error?!?!
      await Deno.readDir(path)[Symbol.asyncIterator]().map(with_base).toArray()
    ).map(get_mod_info),
  )
)
  .filter(fulfilled)
  .map(get_value)

console.log(foo)
console.log(Deno.resources())

// But doing it syncrounously takes ~2.0 seconds
/*
const found_mods = []
for await (const potential of Deno.readDir(path)) {
  const full_path = pathlib.join(path, potential.name)
  try {
    const zipfile = new zip.RandomAccessZipReader(full_path)
    for await (const ent of zipfile.readDir('')) {
      if (ent.isDirectory)
        found_mods.push(
          JSON.parse(await zipfile.readTextFile(ent.name + '/info.json')),
        )
    }
  } catch {}
}
console.log(found_mods)
*/

/*
const path3 = '/home/lexxy/Games/Factorio/mods/factoriohd_logistics_0.0.8.zip'

const zipfile = new zip.RandomAccessZipReader(path3)

const results = zipfile.readDir('/factoriohd_logistics_0.0.8/data/base/graphics/entity')

// @ts-ignore: This was working fine yesterday without an error?!?!
console.log(await results.toArray())
*/
