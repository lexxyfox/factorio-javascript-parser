import Resource from 'https://gitlab.com/snippets/2378298/raw/main/..ts'

export type Opener<T = Deno.FsFile> = {
  open: (path: string | URL, options?: Deno.OpenOptions) => Promise<T>
}

export type DirReader = {
  readDir: typeof Deno.readDir
}

export type FileReader = {
  readFile: typeof Deno.readFile
}

export type TextReader = {
  readTextFile: typeof Deno.readTextFile
}

export type Runner = {
  run: typeof Deno.run
}

export type ReadableFile = Deno.Reader &
  Deno.Closer &
  Deno.Seeker &
  Partial<Resource>
