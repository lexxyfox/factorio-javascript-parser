import { readAll } from 'https://deno.land/std/streams/conversion.ts'
import * as fsimpl from './fsimpl.ts'

// @ts-ignore:
const decode: (buffer: Uint8Array) => string = Deno.core.decode

export default async (
  path: string | URL,
  filesystem:
    | fsimpl.Opener
    | fsimpl.FileReader
    | fsimpl.TextReader = Deno,
) => {
  if ('readTextFile' in filesystem) return filesystem.readTextFile(path)
  if ('readFile' in filesystem) return decode(await filesystem.readFile(path))
  const file = await filesystem.open(path, { read: true })
  try {
    return decode(await readAll(file))
  } finally {
    file.close()
  }
}
