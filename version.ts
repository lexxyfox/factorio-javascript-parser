export const Version = class {
  major: number
  minor: number
  patch?: number

  constructor(major: number, minor: number, patch?: number) {
    this.major = major
    this.minor = minor
    this.patch = patch
  }

  [Symbol.for('Deno.customInspect')]() {
    return typeof this.patch === 'undefined'
      ? `${this.major}.${this.minor}`
      : `${this.major}.${this.minor}.${this.patch}`
  }
}

export const DEFAULT = new Version(0, 12)

export const parse = (ver: string) => {
  const parts = ver.split('.')
  return new Version(
    parseInt(parts[0]),
    parseInt(parts[1]),
    parts.length >= 3 ? parseInt(parts[2]) : undefined,
  )
}
