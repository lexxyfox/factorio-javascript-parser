export default class {
  #start: number
  constructor(greeting: string) {
    const now = new Date()
    console.log(
      '   0.000 ' +
        now.getFullYear().toString().padStart(4, '0') +
        '-' +
        (now.getMonth() + 1).toString().padStart(2, '0') +
        '-' +
        now.getDate().toString().padStart(2, '0') +
        ' ' +
        now.getHours().toString().padStart(2, '0') +
        ':' +
        now.getMinutes().toString().padStart(2, '0') +
        ':' +
        now.getSeconds().toString().padStart(2, '0') +
        '; ' +
        greeting,
    )
    this.#start = +new Date()
  }

  log = (msg: string) =>
    console.log(
      ((+new Date() - this.#start) / 1000).toFixed(3).padStart(8, ' ') +
        ' ' +
        msg,
    )
}
